from django.shortcuts import render,redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .models import Category,Post,User
from django.views.generic import TemplateView, DetailView
from django.views.generic.list import ListView
from .forms import CategoryModelForm,PostModelForm
# Create your views here.


class AllPostsView(ListView):
    model = Post
    template_name = 'posts/index.html'
    queryset = Post.objects.filter(status=100)


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class AddPostView(TemplateView):
    model = PostModelForm

    def get(self, request, *args, **kwargs):
        posts = Post.objects.all()
        form = PostModelForm(initial={'category': 1})
        context = {
            'form': form,
            'posts': posts,
        }
        return render(request, 'posts/add_post.html', context)

    def post(self, request, pk=None):
        if request.method == "POST":
            form = PostModelForm(request.POST)
            form.user = request.user
            if form.is_valid():
                form.save()
                return redirect('/posts')
            return AllPostsView.as_view()


class PostDetailView(DetailView):
    model = Post
    template_name = "posts/post_details.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})
