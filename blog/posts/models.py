from django.db import models
from django.contrib.auth import get_user_model


# Create your models here.

User = get_user_model()


class Category(models.Model):
    name = models.CharField(unique=True, max_length=20)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Post(models.Model):
    STATUS_DRAFT = 0
    STATUS_PUBLISHED = 100

    STATUSES = (
        (STATUS_DRAFT,'Draft'),
        (STATUS_PUBLISHED,'Published'),
    )

    status = models.SmallIntegerField(choices=STATUSES,default=STATUS_DRAFT)
    title = models.CharField(max_length=255,unique=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    content = models.TextField()
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title