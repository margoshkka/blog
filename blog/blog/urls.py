"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin

from django.urls import path
from django.conf.urls import url,include
from posts.views import AllPostsView,PostDetailView,AddPostView,signup



urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^signup/$', signup, name='signup'),
    # url(r'^logout/$', logout_view, name='logout'),
    url(r'^$', AllPostsView.as_view()),
    url(r'^posts$', AllPostsView.as_view()),
    url(r'^posts/(?P<pk>[0-9\-]+)/$', PostDetailView.as_view()),
    url(r'^posts/create', AddPostView.as_view()),
    url(r'^oauth/', include('social_django.urls', namespace='social')),  # <--

    # url(r'^shortly$', AllUrlsView.as_view(),  name='url-list'),
    # url(r'^shortly/top$', TopUrlsView.as_view(),  name='url-list'),
    # url(r'^shortly/create$', AddUrlView.as_view(), name='add-url'),
    # url(r'^shortly/(?P<sh_url>[0-9\-]+)$', follow_url),
    # url(r'^shortly/(?P<pk>[0-9\-]+)/details$', UrlDetailView.as_view(),name='url-details')
]

urlpatterns += [
    url(r'^accounts/', include('django.contrib.auth.urls')),
]